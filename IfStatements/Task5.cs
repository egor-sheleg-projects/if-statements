﻿namespace IfStatements
{
    public static class Task5
    {
        public static int DoSomething(int i)
        {
            int result = i;

            if (i < 0)
            {
                if (i >= -5)
                {
                    result += 5;
                }

                return result;
            }

            if (i > 0)
            {
                if (i <= 5)
                {
                    result -= 5;
                }

                return result;
            }

            return result;
        }
    }
}
